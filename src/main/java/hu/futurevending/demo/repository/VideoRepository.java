package hu.futurevending.demo.repository;

import hu.futurevending.demo.repository.entity.VideoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VideoRepository extends CrudRepository<VideoEntity, String> {

	@Override
	List<VideoEntity> findAll();

	@Override
	Optional<VideoEntity> findById(String s);
}
