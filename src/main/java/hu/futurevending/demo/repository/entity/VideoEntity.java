package hu.futurevending.demo.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

/**
 * The class represents a Video entity to store in database
 */
@Entity(name = "videos")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class VideoEntity {

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;

	@Column(name = "name", columnDefinition = "VARCHAR(255)", unique = true, nullable = false)
	private String name;

	@Column(name = "length", columnDefinition = "INT", length = 5, nullable = false)
	private Integer videoLength;

	@Column(name = "description", columnDefinition = "VARCHAR(1023)", nullable = false)
	private String description;

	@Column(name = "created_at")
	private LocalDate createdAt;

}



