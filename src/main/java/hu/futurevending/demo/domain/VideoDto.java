package hu.futurevending.demo.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VideoDto {

	private String id;

	@JsonProperty(required = true)
	@Size(max = 255)
	@NotNull
	private String name;

	@JsonProperty(required = true)
	@NotNull
	@Max(1440L)
	private Integer videoLength;

	@JsonProperty(required = true)
	@Size(max = 1023)
	private String description;

	private LocalDate createdAt;

}
