package hu.futurevending.demo.controller;

import hu.futurevending.demo.domain.VideoDto;
import hu.futurevending.demo.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.PersistenceException;
import javax.validation.Valid;
import java.util.List;

/**
 * Controller class to handle API requests
 */
@RestController
@RequestMapping("/api")
public class VideoRestController {

	@Autowired
	private VideoService videoService;

	/**
	 * Invokes if GET request is performed on /api/videos
	 * @return the videos found in the database in JSON
	 */
	@GetMapping(value = "/videos")
	public ResponseEntity<List<VideoDto>> getVideos() {
		return ResponseEntity.status(HttpStatus.OK).body(videoService.getAllVideos());
	}

	/**
	 * Invokes if POST request is performed on /api/videos
	 * @return the videos found in the database in JSON
	 */
	@PostMapping(value = "/videos")
	public ResponseEntity<VideoDto> postVideo(@Valid @RequestBody VideoDto videoDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(videoService.createNewVideo(videoDto).orElse(videoDto));
	}

	/**
	 * Invokes if the input data is invalid
	 * @param ex the exception to deal with
	 * @return Some basic information about the problem occurred
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		return ResponseEntity.badRequest().body("Some of the inputs are invalid. Name must be unique," +
				" length must be between 1 and 1440");
	}

	/**
	 * Invokes if the provided video name already exists in the database
	 * @param ex the exception to deal with
	 * @return Some basic information about the problem occurred
	 */
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(PersistenceException.class)
	public ResponseEntity<String> nameAlreadyExistsException(PersistenceException ex) {
		return ResponseEntity.badRequest().body("Name already exists in the database");
	}
}
