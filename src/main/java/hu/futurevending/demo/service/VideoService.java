package hu.futurevending.demo.service;

import hu.futurevending.demo.domain.VideoDto;
import hu.futurevending.demo.repository.VideoRepository;
import hu.futurevending.demo.repository.entity.VideoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service layer to communicate between repository and controller
 */
@Service
public class VideoService {

	@Autowired
	private VideoRepository videoRepository;

	// unused in this project
	/**
	 * @param id the id to retrieve data by
	 * @return the optional value of the found DTO
	 */
	public Optional<VideoDto> getVideoById(String id) {
		Optional<VideoEntity> optionalVideoEntity = videoRepository.findById(id);
		return optionalVideoEntity.map(this::toDto);
	}

	/**
	 * Method to save new data into database
	 * @param videoInput the input object to create the new video from
	 * @return the optional value of the saved DTO
	 */
	public Optional<VideoDto> createNewVideo(VideoDto videoInput) {
		VideoEntity toSave = toEntity(videoInput);
		VideoEntity saved = videoRepository.save(toSave);
		return Optional.of(toDto(saved));
	}

	/**
	 * @return all the videos present in database, empty list if none
	 */
	public List<VideoDto> getAllVideos() {
		return videoRepository.findAll().stream().map(this::toDto).collect(Collectors.toList());
	}

	//
	/**
	 * unused in this project
	 * @param id the id to delete data by
	 */
	public void deleteVideoById(String id) {
		videoRepository.deleteById(id);
	}

	/**
	 * Utility method to convert DTO to Entity
	 * @param videoInput the DTO to convert
	 * @return the entity with added data (ID, createdAt)
	 */
	private VideoEntity toEntity(VideoDto videoInput) {
		return VideoEntity.builder()
				.name(videoInput.getName())
				.description(videoInput.getDescription())
				.videoLength(videoInput.getVideoLength())
				.createdAt(LocalDate.now())
				.build();
	}

	/**
	 * Utility method to convert Entity to DTO
	 * @param saved to entity to convert
	 * @return The DTO based on the input Entity
	 */
	private VideoDto toDto(VideoEntity saved) {
		return VideoDto.builder()
				.id(saved.getId())
				.name(saved.getName())
				.description(saved.getDescription())
				.videoLength(saved.getVideoLength())
				.createdAt(saved.getCreatedAt())
				.build();
	}
}
