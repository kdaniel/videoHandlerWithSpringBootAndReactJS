import React, { useState, useEffect } from 'react';
import axios from 'axios';
import confirm from "reactstrap-confirm";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import NewVideo from './components/newVideo/NewVideo'
import Home from './components/home/Home'

const App = () => {

  interface Video {
    id: string,
    name: string,
    length: number,
    description: string,
    created_at: string
  }

  const [videos, setVideos] = useState([]);

  const [playlist, setPlaylist] = useState([]);

  const [inputVideo, setInputVideo] = useState({
    videoName: '',
    videoLength: '',
    videoDescription: ''
  });

  const [redirect, setRedirect] = useState(false);

  const [alertVisible, setAlertVisible] = useState(false);

  const [invalidInput, setInvalidInput] = useState({
    isVideoNameInvalid: false,
    isVideoLengthInvalid: false,
    isVideoDescriptionInvalid: false
  });

  useEffect(() => {
    axios.get('/api/videos').then(response => setVideos(response.data));
  }, []);

  useEffect(() => {
    const refresh = setInterval( async() => {
      let fetchedVideos = await axios.get('/api/videos');
      if(fetchedVideos.data.length !== videos.length) {
        alert("Az elérhető videók listája változott");
        setVideos(fetchedVideos.data);
      }
    }, 60000);
    return () => clearInterval(refresh);
  }, [videos.length]);

  useEffect(() => {
    allInputsInvalidButEmpty()
  });

  const addToPlayList = (event: React.MouseEvent<HTMLButtonElement>): void => {
    let actualVideo: Video | undefined = videos.find((video: Video) => video.id === event.currentTarget.value);
    if (actualVideo !== undefined && playlist.filter((x: Video) => x.id === actualVideo!.id).length === 0) {
      setPlaylist(playlist.concat(actualVideo));
    }
  }

  const removeFromPlaylist = (event: React.MouseEvent<HTMLButtonElement>): void => {
    let actualVideo: Video | undefined = playlist.find((video: Video) => video.id === event.currentTarget.value);

    confirm({
      title: (
        <>
          Biztosan szeretnéd törölni ezt a videót a lejátszási listából?
        </>
      ),
      message: `${actualVideo!.name}`,
      confirmText: "Igen",
      cancelText: "Mégsem",
      confirmColor: "primary",
      cancelColor: "link text-danger"
    }).then((value: boolean) => value && setPlaylist(playlist.filter(video => video !== actualVideo)));
  }

  const allInputsInvalidButEmpty = () => {
    if ((inputVideo.videoName.length === 0 && inputVideo.videoLength.length === 0 && inputVideo.videoDescription.length === 0) &&
      (invalidInput.isVideoNameInvalid && invalidInput.isVideoLengthInvalid && invalidInput.isVideoDescriptionInvalid)) {
      clearWarning()
    }
  }

  const handleNameChange = (event: React.FormEvent<HTMLInputElement>) => {
    setInputVideo({ ...inputVideo, [event.currentTarget.name]: event.currentTarget.value })
    let isInvalid = videos.filter((video: Video) => video.name === event.currentTarget.value).length > 0 || event.currentTarget.value === '';
    setInvalidInput({ ...invalidInput, isVideoNameInvalid: isInvalid })
  }

  const handleLengthChange = (event: React.FormEvent<HTMLInputElement>) => {
    setInputVideo({ ...inputVideo, [event.currentTarget.name]: event.currentTarget.value })
    let number = parseInt(event.currentTarget.value);
    let isInvalid = isNaN(number) || number > 1440 || number <= 0;
    setInvalidInput({ ...invalidInput, isVideoLengthInvalid: isInvalid })
  }

  const handleDescriptionChange = (event: React.FormEvent<HTMLInputElement>) => {
    setInputVideo({ ...inputVideo, [event.currentTarget.name]: event.currentTarget.value })
    let isInvalid = event.currentTarget.value === "";
    setInvalidInput({ ...invalidInput, isVideoDescriptionInvalid: isInvalid })
  }

  const disableSaving = () => invalidInput.isVideoNameInvalid || invalidInput.isVideoLengthInvalid || invalidInput.isVideoDescriptionInvalid;

  const addNewVideo = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    let videoToSave = {
      "name": event.currentTarget.videoName.value,
      "description": event.currentTarget.videoDescription.value,
      "videoLength": event.currentTarget.videoLength.value
    };
    axios.post('/api/videos', videoToSave).then(response => {
      if (response.status === 201) {
        setInputVideo({
          videoName: '',
          videoLength: '',
          videoDescription: ''
        })
        setVideos(videos.concat(response.data));
        setAlertVisible(true);

        setTimeout(() => {
          setRedirect(true);
          setAlertVisible(false);
          setRedirect(false);
        }, 1500);
      }
    });
  }

  const clearWarning = () => {
    let newInvalidInput = {
      isVideoNameInvalid: false,
      isVideoLengthInvalid: false,
      isVideoDescriptionInvalid: false
    }
    setInvalidInput(newInvalidInput);
  }

  return (
    <Router>
      <Switch>
        {redirect && <Redirect from="/videos/new" to="/" />}
        <Route path="/videos/new">
          <NewVideo
            inputVideo={inputVideo}
            addNewVideo={addNewVideo}
            alertVisible={alertVisible}
            handleNameChange={handleNameChange}
            handleLengthChange={handleLengthChange}
            handleDescriptionChange={handleDescriptionChange}
            disableSaving={disableSaving()}
            invalidInput={invalidInput}
          />
        </Route>
        <Route path="/">
          <Home
            allVideos={videos}
            videosOnPlaylist={playlist}
            handleOnClickSaveToPlaylist={addToPlayList}
            handleOnClickRemoveFromPlaylist={removeFromPlaylist}
          />
        </Route>
      </Switch>
    </Router>
  )
}

export default App;
