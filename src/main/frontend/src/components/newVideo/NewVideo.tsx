import React from 'react';
import { Button, Alert } from 'reactstrap';
import { Link } from "react-router-dom"

import NewVideoForm from './NewVideoForm'

const NewVideo = ({
  inputVideo,
  addNewVideo,
  alertVisible,
  handleNameChange,
  handleLengthChange,
  handleDescriptionChange,
  disableSaving,
  invalidInput
}) => {

  return (
    <div>
      <Alert isOpen={alertVisible} color="success" >Videó sikeresen mentve!</Alert>
      <NewVideoForm
        inputVideo={inputVideo}
        addNewVideo={addNewVideo}
        handleNameChange={handleNameChange}
        handleLengthChange={handleLengthChange}
        handleDescriptionChange={handleDescriptionChange}
        disableSaving={disableSaving}
        invalidInput={invalidInput}
      />
      <Link to="/"><Button color="secondary" >vissza a főoldalra</Button></Link>
    </div>
  )
}

export default NewVideo;
