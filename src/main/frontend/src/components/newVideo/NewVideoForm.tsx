import React from 'react';
import {
  Button,
  Col,
  Row,
  Form,
  FormGroup,
  Label,
  Input,
  FormFeedback
} from 'reactstrap';

const NewVideoForm = ({
  inputVideo,
  handleNameChange,
  handleLengthChange,
  handleDescriptionChange,
  addNewVideo,
  disableSaving,
  invalidInput
}) => {

  const isDisabled = () => disableSaving || 
  (inputVideo.videoName.length === 0 || inputVideo.videoLength.length === 0 || inputVideo.videoDescription.length === 0)

  return (
    <div className="container">
      <h2>Új videó létrehozása</h2>
      <Form onSubmit={addNewVideo}>
        <Row form>
          <Col md={4}>
            <FormGroup>
              <Label for="videoName" >Videó neve:</Label>
              <Input
                invalid={invalidInput.isVideoNameInvalid}
                value={inputVideo.videoName}
                onChange={handleNameChange}
                name="videoName"
                maxLength="255"
                type="text"
                id="videoName"
                placeholder="Add meg a videó nevét"
              />
              <FormFeedback>Adj egyedi nevet a videónak</FormFeedback>
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="videoLength">Videó hossza (perc):</Label>
              <Input
                invalid={invalidInput.isVideoLengthInvalid}
                value={inputVideo.videoLength}
                onChange={handleLengthChange}
                name="videoLength"
                type="number"
                id="videoLength"
                placeholder="videó hossza percben"
                min="0"
              />
              <FormFeedback>Add meg a videó hosszát percben. Maximum 1440 perces videót tudsz feltölteni</FormFeedback>
            </FormGroup>
          </Col>
        </Row>
        <Row form>
          <Col md={8}>
            <FormGroup>
              <Label for="videoDescription">Videó leírása:</Label>
              <Input
                invalid={invalidInput.isVideoDescriptionInvalid}
                value={inputVideo.videoDescription}
                onChange={handleDescriptionChange}
                name="videoDescription"
                type="textarea" id="videoDescription"
                placeholder="Add meg a videó leírását - maximum 1023 karakter"
                rows="13"
                maxLength="1023"
              />
              <FormFeedback>Kötelezően kitöltendő mező</FormFeedback>
            </FormGroup>
          </Col>
        </Row>
        <Row form>
          <Col md={2}>
            <Button title={disableSaving ? "Minden mező kitöltése kötelező" : ""}
              color="primary"
              disabled={isDisabled()}>
              Videó hozzáadása
            </Button>
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default NewVideoForm;
