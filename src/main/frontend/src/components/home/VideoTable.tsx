import React from 'react';
import { Table, Button } from 'reactstrap';
import '../../public/stylesheets/style.css';

const VideoTable = ({ allVideos, handleOnClick, videosOnPlaylist }) => {

  const isButtonDisabled = (video) => videosOnPlaylist.filter(x => x.id === video.id).length > 0;

  return (
    <div className="container">
      {allVideos.length === 0 ?
        <h3>Jelenleg nincs elérhető videó, a videó hozzáadása gombbal lehet videót felvinni</h3>
        :
        <div className="container">
          <div className="table-responsive">
            <h3>Elérhető videók</h3>
            <div className="scrollableDiv">
              <Table >
                <thead className="thead-dark">
                  <tr>
                    <th className="stickyTableHead" scope="col">#</th>
                    <th className="stickyTableHead" scope="col">Videó neve</th>
                    <th className="stickyTableHead" scope="col">Videó leírása</th>
                    <th className="stickyTableHead" scope="col">Videó hossza</th>
                    <th className="stickyTableHead" scope="col">Hozzáadás a Lejátszási listához</th>
                  </tr>
                </thead>
                <tbody>
                  {allVideos.map((video, index) => {
                    return <tr key={index}>
                      <th scope="row">{index + 1}</th>
                      <td className="width25">{video.name}</td>
                      <td className="width50"><p className="maxWidth90">{video.description}</p></td>
                      <td>{video.videoLength}</td>
                      <td>
                        <Button id={video.id}
                          value={video.id}
                          color="primary"
                          onClick={handleOnClick}
                          disabled={isButtonDisabled(video)} >
                          hozzáad
                        </Button>
                      </td>
                    </tr>
                  })}
                </tbody>
              </Table>
            </div>
          </div>
        </div>
      }
    </div>
  );
}

export default VideoTable;
