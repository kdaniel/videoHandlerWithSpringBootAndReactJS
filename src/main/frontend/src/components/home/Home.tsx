import React from 'react';
import { Button } from 'reactstrap';
import { Link } from "react-router-dom"

import VideoTable from './VideoTable'
import Playlist from './Playlist'

const Home = ({ allVideos, videosOnPlaylist, handleOnClickSaveToPlaylist, handleOnClickRemoveFromPlaylist }) => {
  return (
    <div>
      <VideoTable allVideos={allVideos} handleOnClick={handleOnClickSaveToPlaylist} videosOnPlaylist={videosOnPlaylist} />
      <br />
      <div className="container" >
        <Link to="/videos/new">
          <Button color="primary" >videó hozzáadása az elérhető videókhoz</Button>
        </Link>
      </div>
      <br />
      <Playlist videosOnPlaylist={videosOnPlaylist} handleOnClick={handleOnClickRemoveFromPlaylist} videosAvailable={allVideos.length > 0} />
    </div>
  )
}

export default Home;
