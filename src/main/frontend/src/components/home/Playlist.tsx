import React from 'react';
import { Table, Button } from 'reactstrap';
import '../../public/stylesheets/style.css';

const Playlist = ({ videosOnPlaylist, handleOnClick, videosAvailable }) => {

  return (
    <div className="container">
      {videosOnPlaylist.length === 0 ?
        videosAvailable && <h3>A lejátszási lista jelenleg üres</h3>
        :
        <div className="container">
          <div className="table-responsive">
            <h3>Lejátszási lista</h3>
            <div className="scrollableDiv">
              <Table>
                <thead className="thead-dark">
                  <tr>
                    <th className="stickyTableHead" scope="col">#</th>
                    <th className="stickyTableHead" scope="col">Videó neve</th>
                    <th className="stickyTableHead" scope="col">Videó leírása</th>
                    <th className="stickyTableHead" scope="col">Videó hossza</th>
                    <th className="stickyTableHead" scope="col">Eltávolítás a Lejátszási listából</th>
                  </tr>
                </thead>
                <tbody>
                  {videosOnPlaylist.map((video, index) => {
                    return <tr key={index}>
                      <th scope="row">{index + 1}</th>
                      <td className="width25">{video.name}</td>
                      <td className="width50"><p className="maxWidth90">{video.description}</p></td>
                      <td>{video.videoLength}</td>
                      <td>
                        <Button value={video.id}
                          id={video.id}
                          color="danger"
                          onClick={handleOnClick}>
                          eltávolít
                        </Button>
                      </td>
                    </tr>
                  })}
                </tbody>
              </Table>
            </div>
          </div>
        </div>
      }
    </div>
  );
}

export default Playlist;
